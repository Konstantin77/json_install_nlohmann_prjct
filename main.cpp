#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "nlohmann/json.hpp"

struct DatEMonthYear
{
    int _year;
    std::string _month;
};

struct Actorinfo
{
    std::string infoDescrpt;
    std::string name;
    std::string family;
};

struct MovieInfo
{
    std::string MovieName;
    std::string country;
    DatEMonthYear dateIssue;
    std::string Studio_name;
    std::string Writer;
    std::string Pict_director;
    std::string Producer;
    std::vector <Actorinfo> Heroes;
};

struct Movies
{
    MovieInfo Movies_five[5];
};

int main()
{
    Movies record;
    int counter = 0;
    std::string searCh_name = "";
    std::ifstream file("Movies5.json");
    nlohmann::json dict;
    file >> dict;

    for (nlohmann::json::iterator it = dict.begin(); it != dict.end(); ++it)// ключи фильмов в файле все должны быть разные!!
    {
        const auto& movieRef = dict[it.key()];
        auto& Movies_fiveRef = record.Movies_five[counter];

        Movies_fiveRef.MovieName = it.key();
        Movies_fiveRef.country = movieRef["Country"];
        Movies_fiveRef.dateIssue._year = movieRef["Date if Issue"]["year"];
        Movies_fiveRef.dateIssue._month = movieRef["Date if Issue"]["month"];
        Movies_fiveRef.Studio_name = movieRef["Studio name"];
        Movies_fiveRef.Writer = movieRef["Writer"];
        Movies_fiveRef.Pict_director = movieRef["Director"];
        Movies_fiveRef.Producer = movieRef["Producer"];

        for (int i = 0; i < movieRef["actorsInfo"].size(); ++i)
        {
            Actorinfo temp_Actorinfo;
            const auto& movieRef_i = movieRef["actorsInfo"][i];

            temp_Actorinfo.infoDescrpt = movieRef_i["descript"];
            temp_Actorinfo.name = movieRef_i["nameActor"];
            temp_Actorinfo.family = movieRef_i["familyActor"];

            Movies_fiveRef.Heroes.push_back(temp_Actorinfo);
        }

        ++counter;
    }

        ////////////////// поиск
        std::cout << "\n Enter the actor Surname or FirstName for searching inside the base..." << std::endl;
        std::cin >> searCh_name;
        std::cout  << std::endl;

        auto& Movies_fiveRef = record.Movies_five;
        for(int k = 0; k < 5; ++k)// проход по всем записям фильмов
        {

            for (int j = 0; j < Movies_fiveRef[k].Heroes.size(); ++j)// проход по всем характерам в фильме
            {
                if(Movies_fiveRef[k].Heroes[j].family == searCh_name || Movies_fiveRef[k].Heroes[j].name == searCh_name)
                {
                    auto& Movies_Heroes = Movies_fiveRef[k].Heroes[j];
                    std::cout << "The Movie = " <<  Movies_fiveRef[k].MovieName  << std::endl;
                    std::cout << " The Surname = " << Movies_Heroes.family << std::endl;
                    std::cout << " The FirstName = " << Movies_Heroes.name << std::endl;
                    std::cout << " The Charachtr = " << Movies_Heroes.infoDescrpt << std::endl;
                }
            }
            std::cout  << std::endl;
        }
    return 0;
}
